plugins {
    kotlin("jvm")
}

version = "unspecified"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(project(":pureEngine"))
    implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
}
