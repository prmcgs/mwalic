plugins {
    kotlin("multiplatform") version "1.4.10" apply false
    kotlin("jvm") version "1.4.10" apply false
//    id("org.jetbrains.kotlin.android") version "1.4.10" apply false
}
buildscript {
    val kotlin_version by extra("1.4.10")
    dependencies {
        "classpath"("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
    }
}
