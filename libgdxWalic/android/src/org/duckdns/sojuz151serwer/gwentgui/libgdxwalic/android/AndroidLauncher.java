package org.duckdns.sojuz151serwer.gwentgui.libgdxwalic.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import org.duckdns.sojuz151serwer.gwentgui.graphicAdapter.InitialRenderer;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new InitialRenderer("auto"), config);
	}
}
