package org.duckdns.sojuz151serwer.gwentgui.graphicAdapter

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Graphics
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.viewport.ScreenViewport
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import ktx.actors.onClick
import ktx.assets.async.AssetStorage
import ktx.async.KtxAsync
import ktx.async.newAsyncContext
import me.xdrop.fuzzywuzzy.FuzzySearch
import org.duckdns.sojuz151serwer.gwentgui.cards.initGwentBeta
import org.duckdns.sojuz151serwer.gwentgui.cards.startGwentBeta
import org.duckdns.sojuz151serwer.gwentgui.engine.*
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.measureTimeMillis

fun getDeckPatch(): File {
    val path = System.getenv("EXTERNAL_STORAGE") ?: System.getProperty("user.home") //xD
    val patch = Paths.get(path).resolve(("Decks"))
    Files.createDirectories(patch)
    return patch.toFile()
}

val defDecks = listOf("alchemy.wcd", "armor.wcd", "cursed.wcd", "discard.wcd", "machines.wcd", "reveal.wcd", "damage.wcd")
fun getPossibleDecks(): List<Pair<String, List<String>?>> {
    return (listOf(Pair("Random", null)) +
            (getDeckPatch().listFiles()?.filter { it.isFile && it.extension == "wcd" }?.map { Pair("Custom: ${it.name}", it.readLines()) }
                ?: listOf()) +
            defDecks.map { Pair("Deafult: $it ", Gdx.files.internal("Decks/$it").reader().readLines()) })
}

fun deckToNiceDest(content: List<String>?): String {
    return content?.map { it.replace("org.duckdns.sojuz151serwer.gwentgui.cards.", "").trim() }
        ?.filter { it.isNotEmpty() }?.joinToString("\n") ?: "Random cards \n with random leader"
}


val imageMap = mutableMapOf<AbstractCard, TextureRegion>()
val globalMutex = Mutex()


class InitialRenderer(val hostname: String) : ApplicationAdapter() {

    lateinit var stage: Stage
    override fun dispose() {
        stage.dispose()
    }

    var wasInited = false
    override fun create() {
        if (!wasInited) {
            stage = Stage(ScreenViewport())
            Gdx.input.inputProcessor = stage
            defSkin = Skin(Gdx.files.internal("uiskin.json"))
            wasInited = true
        }
        val outerTable = Table(defSkin)
        val tab1 = Table(defSkin)
        outerTable.add(tab1)
        tab1.add(Label("Hostname", defSkin))
        val hostnameField = TextField(hostname, defSkin)
        tab1.add(hostnameField)
        val dc = TextButton("Deck creator", defSkin)
        tab1.add(dc)
        dc.onClick {
            DeckCreator(this.stage, this@InitialRenderer)

        }
        outerTable.row()
        outerTable.width = stage.width
        outerTable.height = stage.height
        val innerTable = Table(defSkin)
        val scrollPanel = ScrollPane(innerTable)
        outerTable.add(scrollPanel)
        stage.addActor(outerTable)
        scrollPanel.width = stage.width
        scrollPanel.height = stage.height
        getPossibleDecks().forEach { (name, content) ->
            val b = TextButton(name + "\n" + deckToNiceDest(content), defSkin)
            innerTable.add(b)
            b.onClick {
                this@InitialRenderer.stage.clear()
                GlobalScope.launch { //TODO fancy
                    startGwentBeta(LibGdxAdapter(this@InitialRenderer.stage, content, hostnameField.text, this@InitialRenderer), this, hostnameField.text)
                }
            }
        }
        KtxAsync.initiate()
        initGwentBeta()
        val lol = getDeckPatch().listFiles()
        if (lol != null) {
            val found = mutableListOf<File>(*lol)
            val images = mutableListOf<Pair<File, String>>()
            while (found.isNotEmpty()) {
                val c = found.removeAt(0)
                if (c.isFile && c.extension == "jpg") {
                    images.add(Pair(c, c.name))
                }
                if (c.isDirectory)
                    found.addAll(c.listFiles()!!)
            }
            val threadCount = Runtime.getRuntime().availableProcessors();
            val assetStorage = AssetStorage(asyncContext = newAsyncContext(threads = threadCount))
            val short = images.map { it.second }
            if (short.isNotEmpty())
                KtxAsync.launch {
                    val t = measureTimeMillis {
                        allPossibleCards.chunked(allPossibleCards.size / threadCount).map { list ->
                            launch {
                                list.forEach { abstractCard ->
                                    assetStorage.apply {
                                        val foundImage = FuzzySearch.extractOne(abstractCard.name, short)
                                        val fh = images[foundImage.index].first.absolutePath
                                        imageMap[abstractCard] = TextureRegion(load<Texture>(fh))
                                    }
                                }
                            }
                        }.forEach { it.join() }
                    }
                    println("All assect loaded in $t miliseconds ")
                }
        }
    }


    override fun resize(width: Int, height: Int) {
        //jebie czciąki
        stage.root.scaleY = height / 1080f
        stage.root.scaleX = width / 1920f
        stage.viewport.update(width, height, true)
    }

    init {
        runBlocking {
            globalMutex.lock()
        }

    }

    override fun render() {

        if (Gdx.input.isKeyJustPressed(Input.Keys.F) && Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            val currentMode = Gdx.graphics.displayMode
            if (Gdx.graphics.isFullscreen) Gdx.graphics.setWindowedMode(currentMode.width, currentMode.height) else Gdx.graphics.setFullscreenMode(currentMode)
        }
        runBlocking {
            globalMutex.unlock()
            globalMutex.lock()
        }
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        stage.act()
        stage.draw()


    }
}
