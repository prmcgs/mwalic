plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization") version "1.4.10"
}

version = "unspecified"

repositories {
    mavenCentral()
}

kotlin {
    jvm()
    js {
        browser {}
    }
    /* Targets configuration omitted. 
    *  To find out how to configure the targets, please follow the link:
    *  https://kotlinlang.org/docs/reference/building-mpp-with-gradle.html#setting-up-targets */

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation ("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.1")
            }
        }
    }
}