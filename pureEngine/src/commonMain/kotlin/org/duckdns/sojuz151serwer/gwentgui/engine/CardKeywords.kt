package org.duckdns.sojuz151serwer.gwentgui.engine

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json


suspend fun Card.destroy(withoutCalling: Boolean = false) {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Destroyed, this))

    if (!withoutCalling) {
        state.callForSingle("onDeath", this) { it.onDeath() }
    }
    val location = this.location()
    state.getRowByLocation(location).remove(this)
    if (tags.contains(Tag.TOKEN) || baseStrength < 1 || ist(Tag.Doomed))
        banish(location.playerId)
    else
        putInGraviard(location.playerId)
}

suspend fun Card.charm() {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Charmed, this))
    val location = this.location()
    state.getRowByLocation(location).remove(this)
    state.getRowByLocation(location.opositeRow()).add(this)
}

suspend fun Card.heal(maxHeal: Int = Int.MAX_VALUE) {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Healed, this))
    currentStrength += (baseStrength - currentStrength).coerceIn(0, maxHeal)

}

suspend fun Card.strengthen(i: Int) {
    val state = state!!
    state.customPrint(State.PrintOption.WithValue(State.PrintOption.ModsWithValue.Strenghten, this, i))
    baseStrength += i
    currentStrength += i
}

suspend fun Card.weaken(i: Int) {
    val state = state!!
    state.customPrint(State.PrintOption.WithValue(State.PrintOption.ModsWithValue.Weeken, this, i))
    baseStrength -= i
    currentStrength -= i
    if (currentStrength <= 0)
        destroy()
}

suspend fun Card.giveArmour(i: Int) {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    state.customPrint(State.PrintOption.WithValue(State.PrintOption.ModsWithValue.Armored, this, i))
    armour += i
}

suspend fun Card.reset() {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Resetet, this))
    currentStrength = baseStrength
}

suspend fun Card.resurrect(playerId: Int, random: Boolean = false) {
    val state = state!!
    (state.players.map { it.graveyard }).forEach { it.remove(this) }
    if (state.players.any { player -> player.rows.any { it.contains(this) } })
        return
    currentStrength = baseStrength
    this.playCardwithBattlecryAndEverything(
        playerId,
        AbstractCard.PlayMod.Resurected,
        forceedLocation = if (random) CardLocation(playerId, state.getRng().nextInt(0, 3)) else null
    )
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Resurected, this))
}

suspend fun Card.move(newLane: Int) {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    val location = this.location()
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Moved, this))
    state.getRowByLocation(location).remove(this)
    location.rowId = newLane
    location.positionId = 0
    state.putCard(this, location)
    state.callForSingle("onMoved", this) { it.onMoved() }
}

suspend fun Card.move(location: CardLocation) {
    move(location.rowId)
}

suspend fun Card.boost(ammount: Int) {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    state.customPrint(State.PrintOption.WithValue(State.PrintOption.ModsWithValue.Boosted, this, ammount))
    currentStrength += ammount
}

suspend fun Card.removeAllArmour(): Int {
    val state = state!!
    if (this.notOnBattlefiled() || armour == 0)
        return 0
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.ArmourRemoved, this))
    val a = armour
    armour = 0
    state.callForSingle("onArmorLost", this) { it.onArmomrLost() }
    return a
}


suspend fun AbstractCard.spawn(state: State, playerId: Int, forceedLocation: CardLocation? = null): Card {
    val ret = Card(this, state.getNextUUID(), state)
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Spawned, ret))
    ret.playCardwithBattlecryAndEverything(playerId, AbstractCard.PlayMod.Spawned, forceedLocation)

    return ret
}

fun Card.removeFromeverywhere() {
    state!!.players.flatMap { it.rows }.forEach { it.remove(this) }
    state!!.players.map { it.graveyard }.forEach { it.remove(this) }
}

enum class DeckLocation {
    MID,
    TOP,
    BOTTOM
}

suspend fun Card.addToDeck(playerId: Int, deckLocation: DeckLocation = DeckLocation.MID) {
    val state = state!!
    removeFromeverywhere()
    state.customPrint(State.PrintOption.AddToDeck(this, playerId))
    state.steamAdapter.applyAction(playerId) { sih ->
        sih.cardToDraw.add(this)
        when (deckLocation) {
            DeckLocation.MID -> Unit
            DeckLocation.TOP -> sih.moveToTop(this)
            DeckLocation.BOTTOM -> sih.moveToBottom(this)
        }
    }
}

suspend fun Card.addToHand(playerId: Int) {
    removeFromeverywhere()
    val cardString = Json.encodeToString(this)
    val state = state!!
    state.steamAdapter.applyAction(playerId) {
        it.cardInHand.add(Json.decodeFromString(cardString))
    }
    state.players[playerId].knownHand.add(this)
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.ReturedToHand, this))

}

internal suspend fun Card.putInGraviard(playerId: Int) {
    state!!.players[playerId].graveyard.add(this)
    state!!.callForSingle("onPutInGraviard", this) { it.onPutInGraviard() }
}

suspend fun Card.discard(playerId: Int) {
    val state = state!!
    //this function assumes card was removed from wheerewere it was
    putInGraviard(playerId)
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Discarded, this))
    state.callForSingle("onDiscarded", this) { it.onDiscarded() }
    state.callForAll("onCardDiscared"){it.onCardDiscared(this)}
}

suspend fun Card.banish(playerId: Int) { //na gdziekowliek
    val state = state!!
    removeFromeverywhere()
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Banished, this))
    state.players[playerId].banished.add(this)
}

suspend fun Card.dealDamage(amount: Int): Boolean {
    val state = state!!
    if (this.notOnBattlefiled())
        return false
    state.customPrint(State.PrintOption.WithValue(State.PrintOption.ModsWithValue.Damaged, this, amount))
    var ammountLeftToDeal = amount
    if (armour != 0) {
        if (ammountLeftToDeal >= armour) {
            ammountLeftToDeal -= armour
            armour = 0
            state.callForSingle("onArmorLost", this) { it.onArmomrLost() }
        } else {
            armour -= ammountLeftToDeal
            return false
        }
    }
    if (ammountLeftToDeal > 0) {
        currentStrength -= ammountLeftToDeal
        if (currentStrength <= 0) {
            destroy()
            return true
        } else {
            state.callForSingle("onDamaged", this) { it.onDamaged() }
            state.callForAll("onAnyCardDamaged") { it.onCardDamaged(this) }

        }
    }
    return false
}

suspend fun Card.playCardwithBattlecryAndEverything(playerId: Int, playMod: AbstractCard.PlayMod, forceedLocation: CardLocation? = null) {
    val state = state!!
    val auxCallData = if (!ist(Tag.SPECIAL)) {
        val location = forceedLocation
            ?: state.steamAdapter.getInput(playerId) { it.pickPlaceBetweenCards(this, if (ist(Tag.SPY)) mEnemy else mAlly) }
        state.putCard(this, location)
        AbstractCard.AuxCallData(this.getNAdjacent(1).count { it.ist(Tag.Crewd) }, playMod)
    } else {
        this.putInGraviard(playerId)
        AbstractCard.AuxCallData(0, playMod)
    }
    state.callForAll("onCardAppear") { it.onCardAppear(this, playMod) }
    state.players.indices.forEach { p ->
        state.players[p].graveyard.toMutableList().forEach { card ->
            state.callForSingle("onCardAppearInGraviard", card) { it.onCardAppearInGraviard(this, playMod) }
        }
    }

    if (!(this.ist(Tag.SINGLEUSE) && this.didUseAbility) && (!this.notOnBattlefiled() || this.ist(Tag.SPECIAL))) {
        this.didUseAbility = true //najpierw bo sie jebie
        state.callForSingle("OnEntry", this) { it.onEntry(auxCallData) }
    }
    state.cardPlayedHistory.add(Pair(this.ac, playerId))
}

fun Card.notOnBattlefiled(): Boolean {
    return !state!!.players.any { p -> p.rows.any { it.contains(this) } }
}


suspend fun Card.toggleLock() {
    val state = state!!
    if (this.notOnBattlefiled())
        return
    state.customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.LockToggled, this))
    locked = !locked
}

suspend fun Card.transform(to: AbstractCard) {
    val oldAc = this.ac
    this.ac = to
    state!!.customPrint(State.PrintOption.Transform(oldAc, this))
}


suspend fun Card.setCurrentStrenght(i: Int) {
    val state = state!!
    state.customPrint(State.PrintOption.WithValue(State.PrintOption.ModsWithValue.CurrentSrenghtSettet, this, i))
    currentStrength = i
    if (i <= 0)
        destroy()
}

suspend fun Card.setBaseStreanght(i: Int) {
    state!!.customPrint(State.PrintOption.WithValue(State.PrintOption.ModsWithValue.BaseSrenghtSettet, this, i))
    baseStrength = i
    currentStrength = i
    if (i == 0)
        destroy()
}

fun Card.wetherUnder(): Weather? {
    val location = location()
    return state!!.players[location.playerId].weathers[location.rowId]
}