package org.duckdns.sojuz151serwer.gwentgui.engine


//warpers aroud  PlayerAdapter
suspend fun State.pickCardOnBattleFiled(playerId: Int, mask: RowMask = mEnemy, filter: (Card) -> Boolean = { true }): Card? {
    val possibilites = getAlCardsOnBattleFiled(playerId, mask).filter(filter)
    if (possibilites.isEmpty())
        return null
    val cardID = steamAdapter.getInput(playerId) { customPrint(State.PrintOption.InfoForPlayer("Pick card on battlefiled "));it.pickFromList(possibilites) }[0]
    return possibilites[cardID]
}

suspend fun State.pickFromGraviard(playerId: Int, targetMods: TargetMods = TargetMods.Ally, filter: (Card) -> Boolean = { true }): Card? {
    val graviards = players.indices.filter { targetMods.checkIfOk(playerId, it) }.map { players[it].graveyard }.flatten().filter(filter)
    if (graviards.isEmpty())
        return null
    val card = graviards[steamAdapter.getInput(playerId) { customPrint(State.PrintOption.InfoForPlayer("Pick card from graviards"));it.pickFromList(graviards) }[0]]
    players.forEach { it.graveyard.remove(card) }
    return card

}

suspend fun State.pickFromOwnHand(playerId: Int, filter: ((Card) -> Boolean) = { true }, random: Boolean = false, canLeaderAndPass: Boolean = false, purpurs: String = ""): Pair<Card, Boolean>? {
    val leaderArray = if (canLeaderAndPass && players[playerId].leader != null) listOf(players[playerId].leader!!) else listOf()
    val (index, card) = steamAdapter.getInfoWithUserInput<List<Int>, Pair<Int, Card>?>(playerId) { eval, sih ->

        val goodCards = sih.cardInHand.filter(filter) + leaderArray //Przyjrzy się temu ale wydaje się działąć bo i tak jest stringowany i dostringowany
        if (goodCards.isEmpty())
            return@getInfoWithUserInput null
        val card = if (random)
            goodCards.random(sih.random)
        else {
            customPrint(State.PrintOption.InfoForPlayer("Pick card from own hand to $purpurs"))
            goodCards[eval { it.pickFromList(goodCards, canLess = canLeaderAndPass) }.elementAtOrElse(0) { return@getInfoWithUserInput null }]
        }
        val index = sih.cardInHand.indexOf(card)
        sih.cardInHand.remove(card)
        return@getInfoWithUserInput Pair(index, card)
    } ?: return null
    val wasRevelad = if (index != -1) {
        val oldCard = players[playerId].knownHand.removeAt(index)
        customPrint(State.PrintOption.Reveled(oldCard, card, false))
        oldCard.ac !is UnknowCard
    } else {
        players[playerId].leader = null
        true
    }
    card.state = this

    return Pair(card, wasRevelad)
}

suspend fun State.pickRow(playerId: Int, mask: RowMask): CardLocation {  //location with last zero

    val cardLocation = steamAdapter.getInput(playerId) { it.pickRow(mask) }
    if (mask.check(playerId, cardLocation))
        return cardLocation
    throw WrongInputException("Wrong argument")
}


suspend fun<A> State.pickNofM(playerId: Int, n: Int, possibilites: List<A>): List<Int> {
    val list = steamAdapter.getInput(playerId) { pa -> pa.pickFromList(possibilites, n) }
    if (list.size == n && list.distinct().size == list.size && list.all { it in possibilites.indices })
        return list
    else
        throw WrongInputException("Inpued list is wrong")
}

suspend fun State.pickIf(playerId: Int, what: String): Boolean {
    return pickNofM(playerId, 1, listOf("$what->No", "$what->Yes"))[0] == 1
}

suspend fun <A> State.pickOneOfCard(playerID: Int, cards: List<A>): A {
    return cards[steamAdapter.getInput(playerID) { it.pickFromList(cards) }[0]]

}

suspend fun State. pickNCardsOnBattlefileD(playerId: Int, rowMask: RowMask,n:Int):List<Card> {
    val all = getAlCardsOnBattleFiled(playerId,rowMask)
    if (all.size<=n)
        return all
    return pickNofM(playerId,n,all).map { all[it] }


}




