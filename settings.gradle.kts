/*
 * This file was generated by the Gradle 'init' task.
 *
 * The settings file is used to specify which projects to include in your build.
 *
 * Detailed information about configuring a multi-project build in Gradle can be found
 * in the user manual at https://docs.gradle.org/6.7/userguide/multi_project_builds.html
 */
//include(":myapplication")
//include(":android")
//include(":myapplication")

rootProject.name = "MWalic"
include("JvmWalic")
include("pureEngine")
include("gwentBeta")
include("libgdxWalic")
include ("libgdxWalic:desktop","libgdxWalic:core","libgdxWalic:ios")
include ("korgeWalic")
//include ("libgdxWalic:android") //uncoment to build for android